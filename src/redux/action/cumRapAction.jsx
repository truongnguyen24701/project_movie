
import { movieServ } from './../../service/movie.Serv';
import { SET_CUM_RAP } from './../constants/cumRapConstants';

export const getCumRapAction = () => {
    return (dispatch) => {
        movieServ
            .getCumRap()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: SET_CUM_RAP,
                    payload: res.data.content
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}