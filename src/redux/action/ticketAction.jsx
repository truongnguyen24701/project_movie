
import { ticketServ } from './../../service/ticket.Serv';
import { DAT_VE, SET_CHI_TIET_PHONG_VE, SET_THONG_TIN_NGUOI_DUNG, DAT_VE_HOAN_TAT, CHUYEN_TAB } from './../constants/ticketConstants';
import { ThongTinDatVe } from './../../Page/Ticket/InforBookTicket';
import { userServ } from './../../service/user.Serv';
import { message } from 'antd';
import { batLoadingAction, tatLoadingAction } from './loadingAction';

export const getTicketAction = (id) => {
    return (dispatch) => {
        ticketServ
            .getTicket(id)
            .then((res) => {
                dispatch({
                    type: SET_CHI_TIET_PHONG_VE,
                    payload: res.data.content
                })
            })
            .catch((err) => {
            })
    }
}

export const getBookTicketAction = (thongTinDatVe = new ThongTinDatVe()) => {
    return (dispatch) => {
         dispatch(batLoadingAction)
         ticketServ
            .DatVe(thongTinDatVe)
            .then((res) => {
                message.success('Đặt vé thành công')
                dispatch({
                    type: DAT_VE,
                    payload: res.data.content,
                    batLoadingAction
                })
                 dispatch(getTicketAction(thongTinDatVe.maLichChieu))
                 dispatch({
                    type: DAT_VE_HOAN_TAT
                })
                dispatch(tatLoadingAction)
                dispatch({
                    type: CHUYEN_TAB
                })
            })
            .catch((err) => {
                message.error(err?.response?.data)
                dispatch(tatLoadingAction)
            })
    }
}

export const getThongTinNguoiDungAction = () => {
    return (dispatch) => {
        userServ
            .layThongTinNguoiDung()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: SET_THONG_TIN_NGUOI_DUNG,
                    payload: res.data.content
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}