export const registerAction = (registerData) => {
    return {
        type: 'REGISTER',
        payload: registerData
    }
}