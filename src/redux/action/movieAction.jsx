
import { SET_MOVIE_LIST } from '../constants/movieConstants';
import { movieServ } from './../../service/movie.Serv';
import { SET_CHI_TIET_PHIM } from './../constants/detailConstants';

export const getMovieListAction = () => {
    return (dispatch) => {
        movieServ
            .getMovieList()
            .then((res) => {
                console.log(res);
                dispatch({
                    type: SET_MOVIE_LIST,
                    payload: res.data.content
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}

export const getDetailPhimAction = (id) => {
    return (dispatch) => {
        movieServ
            .getDetailPhim(id)
            .then((res) => {
                console.log(res);
                dispatch({
                    type: SET_CHI_TIET_PHIM,
                    payload: res.data.content
                })
            })
            .catch((err) => {
                console.log(err);
            })
    }
}