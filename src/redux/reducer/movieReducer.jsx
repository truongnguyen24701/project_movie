
import { SET_CHI_TIET_PHIM } from '../constants/detailConstants';
import { SET_MOVIE_LIST, SET_PHIM_DANG_CHIEU, SET_PHIM_SAP_CHIEU } from './../constants/movieConstants';

const initialState = {
    movieList: [],
    dangChieu: true,
    sapChieu: true,
    movieListDefault: [],


    phimDetail: {}
}

export const movieReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_MOVIE_LIST: {
            state.movieList = payload
            state.movieListDefault = state.movieList
            return { ...state }
        }

        case SET_PHIM_SAP_CHIEU: {
            state.dangChieu = !state.dangChieu
            state.movieList = state.movieListDefault.filter(phim => phim.dangChieu === state.dangChieu)
            return { ...state }
        }

        case SET_PHIM_DANG_CHIEU: {
            state.sapChieu = !state.sapChieu
            state.movieList = state.movieListDefault.filter(phim => phim.sapChieu === state.sapChieu)
            return { ...state }
        }

        case SET_CHI_TIET_PHIM: {
            state.phimDetail = payload
            return { ...state }
        }

        default:
            return state
    }
}