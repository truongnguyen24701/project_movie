import { SET_CUM_RAP } from "../constants/cumRapConstants"

const initialState = {
    heThongCumRap: []
}

export const cumRapReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_CUM_RAP: {
            state.heThongCumRap = payload
            return { ...state }
        }
        default:
            return { ...state }
    }
}