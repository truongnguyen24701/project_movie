import { SET_THONG_TIN_NGUOI_DUNG } from '../constants/ticketConstants';
import { localStorageServ } from './../../service/localSrorageServ';
const initalSate = {
    userInfor: localStorageServ.user.get(),
    thongTinNguoiDung: {

    }
}

export const userReducer = (state = initalSate, { type, payload }) => {
    switch (type) {
        case "LOGIN": {
            state.userInfor = payload;
            return { ...state }
        }
        case SET_THONG_TIN_NGUOI_DUNG: {
            state.thongTinNguoiDung = payload
            return { ...state }
        }
        default:
            return state
    }
}