
import { BAT_LOADING, TAT_LOADING } from './../constants/loadingConstants';


const initialState = {
    isLoading: false
}


export const loadingReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case BAT_LOADING: {
            state.isLoading = true
            return { ...state }

        }
        case TAT_LOADING: {
            state.isLoading = false
            return { ...state }
        }

        default:
            return state
    }
}