
import { DAT_GHE, SET_CHI_TIET_PHONG_VE, DAT_VE_HOAN_TAT, CHUYEN_TAB, CHUYEN_TAB_ACTIVE } from './../constants/ticketConstants';
import { thongTinLichChieu } from './../../Page/Ticket/InforGhe';
import { number } from 'yup/lib/locale';

const initialState = {
    chiTietPhongVe: new thongTinLichChieu(),
    danhSachGheDangChon: [],
    tabActive: '1'
}

export const ticketReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case SET_CHI_TIET_PHONG_VE: {
            state.chiTietPhongVe = payload
            return { ...state }
        }
        case DAT_GHE: {
            console.log('payload', payload);
            let danhSachGheDangChon = [...state.danhSachGheDangChon]
            const isExist = danhSachGheDangChon.includes(payload)
            if (isExist) {
                console.log('isExist', isExist);
                danhSachGheDangChon = danhSachGheDangChon.filter((item) => {
                    return item.maGhe != payload.maGhe
                })
            } else {
                danhSachGheDangChon.push(payload)
            }
            return { ...state, danhSachGheDangChon }
        }
        case DAT_VE_HOAN_TAT: {
            state.danhSachGheDangChon = []
            return { ...state }
        }

        case CHUYEN_TAB: {
            state.tabActive = '2'
            return { ...state }
        }

        case CHUYEN_TAB_ACTIVE: {
            state.tabActive = number
            return { ...state }
        }
        default:
            return state
    }
}