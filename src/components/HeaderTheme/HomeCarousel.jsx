import { Carousel } from 'antd';
import React, { useEffect, useState } from 'react';
import { bannerServ } from './../../service/banner.Serv';

const contentStyle = {
    height: '750px',
    color: '#fff',
    lineHeight: '160px',
    textAlign: 'center',
    backgroundPosition: 'center',
    backgroundSize: '100%',
    backgroundRepeat: 'no-repeat'
};

export default function HomeCarousel() {

    const [banner, setBanner] = useState([])

    useEffect(() => {
        bannerServ
            .getBannerList()
            .then((res) => {
                console.log(res);
                setBanner(res.data.content)
            })
            .catch((err) => {
                console.log(err);
            })
    }, [])
    console.log("banenr", banner);



    const renderBanner = () => {
        return banner.map((item, index) => {
            return <div key={index}>
                <div style={{ ...contentStyle, backgroundImage: `url(${item.hinhAnh})` }}>
                    <img src={item.hinhAnh} className="w-full opacity-0" alt="" />
                </div>
            </div>
        })
    }

    return (
        <Carousel autoplay style={{ overflow: 'hidden' }}>
            {renderBanner()}

        </Carousel>
    )
};
