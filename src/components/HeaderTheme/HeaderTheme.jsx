import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Menu, Space } from 'antd';
import React, { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux';
import { localStorageServ } from './../../service/localSrorageServ';
import { loginAction } from './../../redux/action/loginAction';

export default function HeaderTheme() {

    let history = useNavigate()
    let dispatch = useDispatch()

    let userInfor = useSelector((state) => {
        return state.userReducer.userInfor
    })

    const renderRemove = () => {
        localStorageServ.user.remove()
        dispatch(loginAction(null))
        history('/')
    }

    console.log(userInfor);



    const [open, setOpen] = useState(false);

    const handleMenuClick = (e) => {
        if (e.key === '3') {
            setOpen(false);
        }
    };

    const handleOpenChange = (flag) => {
        setOpen(flag);
    };

    const navLinks = userInfor ? [{
        label: (
            <a>
                {userInfor.hoTen}
            </a>
        ),
        key: '4',
    },
    {
        label: (
            <a
                onClick={renderRemove}
                href="/login">
                Đăng xuất
            </a>
        ),
        key: '5',
    },
    ] : [{
        label: (
            <a onClick={() => {
                window.location.href = "/login"
            }}>
                Đăng nhập
            </a>
        ),
        key: '6'
    },
    {
        label: (
            <a href="/register">
                Đăng ký
            </a>
        ),
        key: '7'
    },]

    const menu = (
        <Menu
            onClick={handleMenuClick}
            items={[
                {
                    label: (
                        <a href='/'>
                            Home
                        </a>
                    ),
                    key: '1',
                },
                {
                    label: (
                        <a href='/contact'>
                            Contact
                        </a>
                    ),
                    key: '2',
                },
                {
                    label: (
                        <a href='/news'>
                            News
                        </a>
                    ),
                    key: '3',
                },
                ...navLinks
            ]}
        />
    );

    return (
        <div>

            <header className="p-4 bg-gray-800 text-gray-100 bg-opacity-40 bg-black text-white fixed w-full z-10 px-20">
                <div className="container flex justify-between h-16 mx-auto">
                    <NavLink to={'/'} className="flex items-center">
                        <img src="https://moveek.com/bundles/ornweb/img/logo.png" style={{ maxHeight: "1.5rem", maxWidth: "100%" }} alt="" />
                    </NavLink>
                    <ul className="items-stretch hidden space-x-3 lg:flex">
                        <li className="flex">
                            <NavLink to="/" className="flex items-center px-4 -mb-1 border-b-2 border-transparent font-bold isActive"
                                style={({ isActive }) => (
                                    isActive ? { color: "#FF884B" } : { color: "white" }
                                )} >Home</NavLink>
                        </li>
                        <li className="flex">
                            <NavLink to="/contact" className="flex items-center px-4 -mb-1 border-b-2 border-transparent isActive"
                                style={({ isActive }) => (
                                    isActive ? { color: "#00FFD1" } : { color: "white" }
                                )} >Contact</NavLink>
                        </li>
                        <li className="flex">
                            <NavLink to="/news" className="flex items-center px-4 -mb-1 border-b-2 border-transparent isActive"
                                style={({ isActive }) => (
                                    isActive ? { color: "#00FFD1" } : { color: "white" }
                                )} >News</NavLink>
                        </li>

                    </ul>
                    <div className="items-center flex-shrink-0 hidden lg:flex">
                        {userInfor ? userInfor.hoTen : <button

                            onClick={() => {
                                window.location.href = '/login'
                            }}
                            className="self-center px-8 py-3 rounded">

                            Đăng Nhập</button>}

                        {userInfor ? <button

                            onClick={() => {
                                { renderRemove() }
                            }}
                            className="self-center px-8 py-3 font-semibold rounded bg-red-400 text-white ml-3">

                            Đăng Xuất</button> : <button
                                onClick={() => {
                                    window.location.href = '/register'
                                }}
                                className="self-center px-8 py-3 font-semibold rounded bg-red-400 text-white">Đăng ký</button>}

                    </div>

                    <button className="p-4 lg:hidden">
                        <Dropdown overlay={menu} onOpenChange={handleOpenChange} open={open}>
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" className="w-6 h-6 text-gray-100">
                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 6h16M4 12h16M4 18h16"></path>
                            </svg>
                        </Dropdown>
                    </button>
                </div>
            </header>
        </div>
    )
}
