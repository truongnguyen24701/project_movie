import { https } from "./config"


export const userServ = {
    postLogin: (loginData) => {
        return https.post("/api/QuanLyNguoiDung/DangNhap", loginData)
    },
    postRegister: (registerData) => {
        return https.post("/api/QuanLyNguoiDung/DangKy", registerData)
    },
    layThongTinNguoiDung: () => {
        return https.post('/api/QuanLyNguoiDung/ThongTinTaiKhoan')
    }
}