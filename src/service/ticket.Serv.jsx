import { https } from "./config"
import { ThongTinDatVe } from './../Page/Ticket/InforBookTicket';

export const ticketServ = {
    getTicket: (maLichChieu) => {
        return https.get(`/api/QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`)
    },
    DatVe: (thongTinDatVe = new ThongTinDatVe()) => {
        return https.post(`/api/QuanLyDatVe/DatVe`, thongTinDatVe)
    },
    
}