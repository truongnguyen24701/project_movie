import { https } from './config'


export const movieServ = {
    getMovieList: () => {
        return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04")
    },
    getCumRap: () => {
        return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP04")
    },
    getDetailPhim: (maPhim) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?maPhim=${maPhim}`)
    }
}