import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Page/Home/Home';
import Contact from './Page/Contact/Contact';
import News from './Page/News/News';
import Login from './Page/Login/Login';
import Register from './Page/Register/Register';
import NotFound from './Page/NotFound/NotFound';
import Admin from './Page/Admin/Admin';
import Ticket from './Page/Ticket/Ticket';
import { Suspense, lazy } from 'react';
import Loading from './components/Loading/Loading';


const LazyDetail = lazy(() => import('./Page/Detail/Detail'))
function App() {
  return (
    <div className="App">
      <Loading />
      <BrowserRouter>
        <Routes>
          <Route path='/' exact element={<Home />} />
          <Route path='*' exact element={<NotFound />} />
          <Route path='/contact' exact element={<Contact />} />
          <Route path='/news' exact element={<News />} />
          <Route path='/detail/:id' exact element={
            <Suspense fallback='LOADING ....'>
              <LazyDetail />
            </Suspense>
          } />
          <Route path="/login" exact element={<Login />} />
          <Route path='/register' exact element={<Register />} />
          <Route path='/admin' exact element={<Admin />} />
          <Route path='/ticket/:id' exact element={<Ticket />} />
        </Routes>
      </BrowserRouter>

      <p>Test commit</p>
    </div>
  );
}

export default App;
