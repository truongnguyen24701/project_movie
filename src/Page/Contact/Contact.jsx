import React from 'react'
import HeaderTheme from '../../components/HeaderTheme/HeaderTheme'
import Footer from './../../components/Footer/Footer';
import HomeCarousel from './../../components/HeaderTheme/HomeCarousel';

export default function Contact() {
    return (
        <div>
            <HeaderTheme />
            <HomeCarousel />
            <div className='bg-green-500'>Đây là Contact</div>
            <Footer />
        </div>
    )
}
