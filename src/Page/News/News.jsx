import React from 'react'
import HeaderTheme from '../../components/HeaderTheme/HeaderTheme'
import Footer from './../../components/Footer/Footer';
import HomeCarousel from './../../components/HeaderTheme/HomeCarousel';

export default function News() {
    return (
        <div>
            <HeaderTheme />
            <HomeCarousel />
            <div className='bg-violet-500'>News</div>
            <Footer />
        </div>
    )
}
