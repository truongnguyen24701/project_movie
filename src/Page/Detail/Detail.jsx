import React, { useEffect } from 'react'
import Footer from '../../components/Footer/Footer';
import HeaderTheme from './../../components/HeaderTheme/HeaderTheme';
import { CustomCard } from '@tsamantanis/react-glassmorphism'
import '@tsamantanis/react-glassmorphism/dist/index.css'
import '../../assets/circle.scss'
import { Rate, Tabs } from 'antd';
import TabPane from 'antd/lib/tabs/TabPane';
import { useSelector, useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { getDetailPhimAction } from '../../redux/action/movieAction';
import moment from 'moment';
import { NavLink } from 'react-router-dom';


export default function Detail() {
    //lấy thông tin param từ url
    const { id } = useParams()
    console.log('id', id);
    const dispatch = useDispatch()
    const phimDetail = useSelector(state => state.movieReducer.phimDetail)
    console.log({ phimDetail });
    useEffect(() => {
        dispatch(getDetailPhimAction(id))
    }, [])

    return (
        <div>
            <HeaderTheme />
            <div style={{ backgroundImage: `url(${phimDetail.hinhAnh})`, backgroundSize: '100%', backgroundPosition: 'center', minHeight: '100vh' }} >
                <CustomCard
                    style={{ paddingTop: 200, minHeight: '100vh', }}
                    effectColor="#fff" // required
                    color="#fff" // default color is white
                    blur={20} // default blur value is 10px
                    borderRadius={0} // default border radius value is 10px
                >
                    <div className='grid grid-cols-12'>
                        <div className='col-span-6 col-start-3'>
                            <div className='grid gird-cols-3 ' style={{ display: 'flex' }}>
                                <img className='col-span-1' src={phimDetail.hinhAnh} style={{ width: 200, height: 300 }} alt="" />
                                <div className='col-span-2 ml-8 mt-10 mr-3'>
                                    <p className='text-sm '>Ngày chiếu: {moment(phimDetail.ngayKhoiChieu).format('DD.MM.YYYY')}</p>
                                    <p className='text-4xl mb-4'>{phimDetail.tenPhim}</p>
                                    <p className=' '>{phimDetail.moTa}</p>
                                </div>
                            </div>
                        </div>

                        <div className='col-span-4'>
                            <h1 className='text-yellow-400 text-2xl font-bold' style={{ marginLeft: 60 }}>Đánh giá</h1>
                            <h1 style={{ marginLeft: 45 }}><Rate allowHalf defaultValue={phimDetail.danhGia / 2} /></h1>
                            <div className={`c100 p${phimDetail.danhGia * 10} big`}>
                                <span className='text-white' style={{ paddingBottom: 20 }}>{phimDetail.danhGia * 10}%</span>
                                <div className="slice">
                                    <div className="bar"></div>
                                    <div className="fill"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className='mt-20 container mx-auto p-5 '>
                        <Tabs tabPosition={'left'} style={{ height: 350, overflow: 'auto' }}>
                            {phimDetail.heThongRapChieu?.map((heThongRap, index) => {
                                return <TabPane tab={<div className='flex text-white'>
                                    <img className='w-20' src={heThongRap.logo} />
                                    <p className='mt-7 ml-2'>{heThongRap.tenHeThongRap}</p>
                                </div>}
                                    key={index}>
                                    {heThongRap.cumRapChieu?.map((cumRap, index) => {
                                        return <div key={index} >
                                            <div className='flex flex-row' >
                                                <img src={cumRap.hinhAnh} className='w-20 m-2' alt="" />
                                                <div>
                                                    <p className='text-2xl font-bold flex mb-3 mt-2'>{cumRap.tenCumRap}</p>
                                                    <p className='text-white flex ' style={{ marginTop: 0 }}>{cumRap.diaChi}</p>
                                                </div>
                                            </div>

                                            <div className='thong-tin-lich-chieu grid grid-cols-4 ml-10 text-2xl mb-4 font-medium text-green-500'>
                                                {cumRap.lichChieuPhim?.slice(0, 8).map((lichChieu, index) => {
                                                    return <NavLink to={`/ticket/${lichChieu.maLichChieu}`} key={index} className='col-span-1'>
                                                        {moment(lichChieu.ngayChieuGioChieu).format('hh:mm A')}
                                                    </NavLink>
                                                })}
                                            </div>
                                        </div>
                                    })}

                                </TabPane>
                            })}
                        </Tabs>
                    </div>
                </CustomCard>
            </div >
            <Footer />
        </div >
    )
}
