import React from "react";
import Slider from "react-slick";
import Flip_Phim from "../Flip_Phim";
import styleSlick from './MultipleRowSlick.module.css'
import { useDispatch } from 'react-redux';
import { SET_PHIM_DANG_CHIEU, SET_PHIM_SAP_CHIEU } from "../../../../redux/constants/movieConstants";


function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={`${className} styleSlick ${styleSlick['slick-prev']}`}
            style={{ ...style, display: "block" }}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={`${className} styleSlick ${styleSlick['slick-prev']}`}
            style={{ ...style, display: "block", left: '-50px' }}
            onClick={onClick}
        />
    );
}




const MultipleRowSlick = (props) => {

    const dispatch = useDispatch()

    const renderPhim = () => {
        return props.movieList.map((item, index) => {
            return (
                <div className="mt-2" key={index}>
                    <Flip_Phim item={item} />
                </div>
            )
        })
    }



    const settings = {
        className: "center variable-width",
        centerMode: true,
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 2,
        speed: 500,
        rows: 2,
        slidesPerRow: 2,
        variableWidth: true,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />

    };
    return (
        <div className="container mx-auto">
            <div className="flex mx-20">
                <button
                    onClick={() => {
                        const action = { type: SET_PHIM_DANG_CHIEU }
                        dispatch(action)
                    }}
                    className="px-8 py-3 font-semibold rounded bg-gray-800 text-white mr-10"
                >PHIM ĐANG CHIẾU</button>
                <button
                    onClick={() => {
                        const action = { type: SET_PHIM_SAP_CHIEU }
                        dispatch(action)
                    }}
                    className="relative overflow-hidden font-semibold rounded px-8 py-3 font-semibold 
                    rounded bg-white text-gray-800 border-gray-800 border"
                >PHIM SẮP CHIẾU
                    <span className="absolute top-0 right-0 px-5 py-1 text-xs tracking-wider text-center uppercase whitespace-no-wrap origin-bottom-left transform 
                        rotate-45 -translate-y-full translate-x-1/3 bg-violet-400">New</span>
                </button>
            </div>
            <Slider {...settings} style={{ marginInline: '70px' }}>
                {renderPhim()}
            </Slider>
        </div>
    );
}


export default MultipleRowSlick;