import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Footer from '../../components/Footer/Footer'
import HeaderTheme from '../../components/HeaderTheme/HeaderTheme'
import { getMovieListAction } from './../../redux/action/movieAction';
import MultipleRowSlick from './ListPhim/RSlick/MultipleRowSlick';
import CumRap from './CumRap/CumRap'
import { getCumRapAction } from './../../redux/action/cumRapAction';
import HomeCarousel from '../../components/HeaderTheme/HomeCarousel';
import { useMediaQuery } from 'react-responsive'

export default function Home() {

    const isDesktopOrLaptop = useMediaQuery({ minWidth: 1224 })
    const isBigScreen = useMediaQuery({ minWidth: 1824 })
    const isTabletOrMobile = useMediaQuery({ maxWidth: 1224 })
    const isPortrait = useMediaQuery({ orientation: 'portrait' })
    const isRetina = useMediaQuery({ minResolution: '2dppx' })

    let dispatch = useDispatch()
    // danh sách phim
    let { movieList } = useSelector((state) => {
        return state.movieReducer;
    })

    useEffect(() => {
        dispatch(getMovieListAction())
    }, [])

    //Hệ thống rạp
    let { heThongCumRap } = useSelector((state) => {
        return state.cumRapReducer
    })

    useEffect(() => {
        dispatch(getCumRapAction())
    }, [])

    return (
        <div>
            <HeaderTheme />
            <HomeCarousel />
            <div className='container mx-auto'>
                <section class="text-gray-600 body-font">
                    <div class="container px-5 py-24 mx-auto">
                        <MultipleRowSlick movieList={movieList} />
                    </div>
                </section>

                <CumRap heThongCumRap={heThongCumRap} />
            </div>
            <Footer />
        </div>
    )
}
