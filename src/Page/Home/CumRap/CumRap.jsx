import React, { Fragment, useState } from 'react'
import { Tabs } from 'antd';
import { NavLink } from 'react-router-dom';
import moment from 'moment'


const { TabPane } = Tabs


export default function CumRap(props) {


    console.log("props", props);

    const [state, setState] = useState({
        tabPosition: "left"
    })



    const changeTabPosition = (e) => {
        setState(e.target.value);
    };



    const renderHeThongRap = () => {
        return props.heThongCumRap?.map((heThongRap, index) => {
            let { tabPosition } = state
            return (
                <TabPane tab={<img className='w-20' src={heThongRap.logo} />} key={index}>
                    <Tabs tabPosition={tabPosition} style={{ height: 650, overflow: 'auto' }} >
                        {heThongRap.lstCumRap?.map((cumRap, index) => {
                            return (
                                <TabPane style={{ height: 650, overflow: 'auto' }} tab={
                                    <div className='text-left w-60 truncate'>
                                        <p className='text-green-700'>{cumRap.tenCumRap}</p>
                                        <p className='truncate'>{cumRap.diaChi}</p>
                                        <button className='text-red-600'>[ Xem chi tiết]</button>
                                    </div>
                                } key={index}>
                                    {cumRap.danhSachPhim.map((phim, index) => {
                                        return <Fragment key={index}>
                                            <div className='flex space-x-10 p-5' >
                                                <img className='w-24 h-48 object-cover my-3' src={phim.hinhAnh} alt={phim.tenPhim} />
                                                <div className='ml-2'>
                                                    <h1 className='font-medium text-2xl text-green-800 flex pt-3'>{phim.tenPhim}</h1>
                                                    <p className='flex py-4'>{cumRap.diaChi}</p>
                                                    <div className='grid grid-cols-5 gap-6'>
                                                        {phim.lstLichChieuTheoPhim?.slice(0, 12).map((lichChieu, index) => {
                                                            return <NavLink to={`/detail/${phim.maPhim}`} className='text-2xl text-blue-500' key={index}>
                                                                {moment(lichChieu.ngayChieuGioChieu).format('hh:mm A')}
                                                            </NavLink>
                                                        })}
                                                    </div>
                                                </div>
                                            </div>
                                            <hr />
                                        </Fragment>
                                    })}

                                </TabPane>
                            )
                        })}

                    </Tabs>
                </TabPane>
            )
        })
    }

    const { tabPosition } = state
    return (
        <>
            <Tabs tabPosition={tabPosition} className='mx-20 my-40'>
                {renderHeThongRap()}
            </Tabs>
        </>
    )
}
