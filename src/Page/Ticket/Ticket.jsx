import React, { Fragment, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import { getBookTicketAction, getThongTinNguoiDungAction, getTicketAction } from '../../redux/action/ticketAction';
import style from './Ticker.module.css'
import { useParams } from 'react-router';
import './Ticket.css'
import { UserOutlined, TeamOutlined, UserAddOutlined } from '@ant-design/icons'
import { CHUYEN_TAB_ACTIVE, DAT_GHE } from '../../redux/constants/ticketConstants';
import sortBy from 'lodash/sortBy'
import { useNavigate } from 'react-router-dom';
import { ThongTinDatVe } from './InforBookTicket';
import { Tabs } from 'antd';
import moment from 'moment';
import { first } from 'lodash';



function Ticket(props) {
    let history = useNavigate()
    const { id } = useParams()
    console.log('id', id);
    let { userInfor } = useSelector((state) => {
        return state.userReducer
    })

    let { chiTietPhongVe, danhSachGheDangChon } = useSelector((state) => {
        return state.ticketReducer
    })

    console.log(danhSachGheDangChon);

    console.log('chiTietPhongVe', chiTietPhongVe);

    let dispatch = useDispatch()

    useEffect(() => {
        dispatch(getTicketAction(id))
    }, [])


    const { thongTinPhim, danhSachGhe } = chiTietPhongVe

    const renderGhe = () => {
        return danhSachGhe.map((ghe, index) => {
            let classGheVip = ghe.loaiGhe === 'Vip' ? 'gheVip' : ''
            let classGheDaDat = ghe.daDat === true ? 'gheDaDat' : ''
            let ClassGheDangDat = danhSachGheDangChon.includes(ghe) ? 'gheDangDat' : ''


            let classGheDaDuocDat = ''
            if (userInfor.taiKhoan === ghe.taiKhoanNguoiDat) {
                classGheDaDuocDat = 'gheDaDuocDat'
            }

            return <Fragment key={index}>
                <button
                    onClick={() => {
                        dispatch({
                            type: DAT_GHE,
                            payload: ghe
                        })
                    }}
                    disabled={ghe.daDat} className={`ghe ${classGheVip} ${classGheDaDat} ${ClassGheDangDat} ${classGheDaDuocDat}`}>
                    {ghe.daDat ? classGheDaDuocDat != '' ? <TeamOutlined style={{ marginBottom: 7, fontWeight: 'bold' }} /> :
                        <UserOutlined style={{ marginBottom: 7, fontWeight: 'bold' }} /> :
                        ghe.stt}
                </button>

            </Fragment >

        })
    }

    return (
        <div className='px-2 sm:px-20 sm:pt-6'>
            <div className='grid grid-cols-12'>
                <div className='col-span-12 w-full sm:col-span-12 lg:col-span-8'>
                    <div className='flex flex-col items-center mt-5'>
                        <div className='bg-black' style={{ width: '100%', height: 15 }} >
                        </div>
                        <div className={`${style['trapezoid']} text-center`}>
                            <h3 className='mt-3 text-black'>Màn hình</h3>
                        </div>
                        <div className='text-center'>
                            {renderGhe()}
                        </div>
                    </div>
                    <div className='mt-5 flex justify-center'>
                        <table className="w-2/3">
                            <thead className="border-b">
                                <tr>
                                    <th>Ghế chưa đặt</th>
                                    <th>Ghế đang đặt</th>
                                    <th>Ghế đã đặt</th>
                                    <th>Ghế VIP</th>
                                    <th>Ghế mình đặt</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th> <button className='ghe text-center'></button></th>
                                    <th> <button className='ghe gheDangDat text-center'></button></th>
                                    <th> <button className='ghe gheDaDat text-center'><UserOutlined style={{ marginBottom: 7, fontWeight: 'bold' }} /></button></th>
                                    <th> <button className='ghe gheVip text-center'></button></th>
                                    <th> <button className='ghe gheDaDuocDat text-center'><TeamOutlined style={{ marginBottom: 7, fontWeight: 'bold' }} /></button></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className='w-80 sm:w-96 sm:ml-10 sm:mt-5'>
                    <h3 className='text-center text-green-400 text-2xl'>
                        {danhSachGheDangChon.reduce((tongTien, ghe) => {
                            return tongTien += ghe.giaVe
                        }, 0)} đ</h3>
                    <hr />
                    <div style={{ fontSize: 15 }}>
                        <div className='flex justify-between '>
                            <p className='text-blue-500'>Tên phim</p>
                            <p className=' font-bold text-red-600'>{thongTinPhim.tenPhim}</p>
                        </div>
                        <div className='flex justify-between my-3 '>
                            <p className='text-blue-500'>Địa điểm</p>
                            <p> {thongTinPhim.diaChi}</p>
                        </div>
                        <div className='flex justify-between  my-3'>
                            <p className='text-blue-500'>Ngày chiếu</p>
                            <p>{thongTinPhim.ngayChieu}</p>
                        </div >
                        <div className='flex justify-between '>
                            <p className='text-blue-500'>Giờ chiếu & Rạp chiếu</p>
                            <p>{thongTinPhim.gioChieu} - {thongTinPhim.tenRap}</p>
                        </div>
                        <hr />
                        <div className='flex justify-between my-3'>
                            <p className='text-red-400 w-4/6'>
                                Ghế
                                {sortBy(danhSachGheDangChon, ['stt']).map((ghe, index) => {
                                    return <span className='text-green-500 text-lg mx-2' key={index}>{ghe.stt}</span>
                                })}
                            </p>
                            <p className='text-green-400 text-lg'>
                                {danhSachGheDangChon.reduce((tongTien, ghe) => {
                                    return tongTien += ghe.giaVe
                                }, 0)} đ
                            </p>
                        </div>
                        <hr />
                        <div className='flex justify-between my-3'>
                            <i className='text-blue-500'>Email</i>
                            {userInfor?.email}
                        </div>
                        <hr />
                        <div className='flex justify-between my-3'>
                            <i className='text-blue-500'>Phone</i>
                            {userInfor?.soDT}
                        </div>
                        <hr />
                    </div>

                    <div className='pt-60 flex flex-col justify-end'>
                        <div
                            onClick={() => {
                                const thongTinDatVe = new ThongTinDatVe()
                                thongTinDatVe.maLichChieu = id
                                thongTinDatVe.danhSachVe = danhSachGheDangChon
                                console.log('thongTinDatVe', thongTinDatVe);
                                dispatch(getBookTicketAction(thongTinDatVe))
                            }}
                            className='bg-green-600 text-white w-full text-center p-2 font-bold text-2xl cursor-pointer'>
                            ĐẶT VÉ
                        </div>

                    </div>
                </div>
            </div>
        </div >
    )
}


const { TabPane } = Tabs

function callback(key) {
    console.log(key);
}

export default function TicketTab(props) {
    let dispatch = useDispatch()
    const { tabActive } = useSelector((state) => {
        return state.ticketReducer
    })
    console.log('tabActive', tabActive);

    return <div className='p-5'>
        <Tabs defaultActiveKey={tabActive} activeKey={tabActive} onChange={(key) =>
            dispatch({
                type: CHUYEN_TAB_ACTIVE,
                number: key
            })
        }>
            <TabPane tab="01 CHỌN GHẾ & THANH TOÁN" key="1">
                < Ticket {...props} />
            </TabPane>
            <TabPane tab="02 KẾT QUẢ ĐẶT VÉ" key="2">
                <KetQuaDatVe {...props} />
            </TabPane>
        </Tabs>
    </div>

}


function KetQuaDatVe(props) {
    let dispatch = useDispatch()
    const { thongTinNguoiDung } = useSelector((state) => {
        return state.userReducer
    })

    useEffect(() => {
        dispatch(getThongTinNguoiDungAction())
    }, [])

    console.log('thongTinNguoiDung', thongTinNguoiDung);

    const renderThongTinDatVe = function () {
        return thongTinNguoiDung.thongTinDatVe?.map((item, index) => {
            const seats = first(item.danhSachGhe)
            return <div key={index} className="h-full flex items-center border-gray-200 border p-4 rounded-lg">
                <img alt="team" className="w-52 h-52 bg-gray-100 object-cover object-center flex-shrink-0  mr-4"
                    src={item.hinhAnh} />
                <div className="flex-grow">
                    <h2 className="text-gray-900 title-font font-medium">{item.tenPhim}</h2>
                    <p className="text-gray-500">Giờ chiếu: {moment(item.ngayDat).format('hh:mm')} -
                        Ngày chiếu: {moment(item.ngayDat).format('DD-MM-YYYY')}</p>
                    <p>Địa điểm: {seats.tenHeThongRap} </p>
                    <p>Tên rạp:  {seats.tenCumRap} </p>
                    <p> Ghế: {item.danhSachGhe.map((ghe, index) => {
                        return <button className='text-green-400 px-2' key={index}>{ghe.tenGhe}</button>
                    })}</p>
                </div>
            </div>
        })
    }

    return <div className='p-5'>
        <section className="text-gray-600 body-font">
            <div className="container px-5 py-24 mx-auto">
                <div className="flex flex-col text-center w-full mb-20">
                    <h1 className="text-4xl font-medium title-font mb-4 text-red-900">Lịch sử đặt vé khách hàng</h1>
                    <p className="lg:w-2/3 mx-auto leading-relaxed text-base">Hãy xem thông tin địa điểm và thười gian để xem phim vui vẻ bạn nhé!</p>
                </div>
                <div className="flex flex-wrap -m-2">
                    <div className="h-56 w-full text-lg">
                        {renderThongTinDatVe()}
                    </div>
                </div>
            </div>
        </section>

    </div>
}

